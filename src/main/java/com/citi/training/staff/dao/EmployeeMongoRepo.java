package com.citi.training.staff.dao;

//import java.util.List;

import com.citi.training.staff.model.Employee;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface EmployeeMongoRepo extends MongoRepository<Employee, String> {
    
    //List<Employee> findByAdress(String address);
}
