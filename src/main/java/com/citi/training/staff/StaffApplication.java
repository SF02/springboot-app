package com.citi.training.staff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StaffApplication {

	// A comment
	public static void main(String[] args) {
		SpringApplication.run(StaffApplication.class, args);
	}

}
