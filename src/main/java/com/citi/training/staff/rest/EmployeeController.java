package com.citi.training.staff.rest;

import java.util.List;

import com.citi.training.staff.service.EmployeeService;
import com.citi.training.staff.model.Employee;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController
@RequestMapping("/v1/employee")
public class EmployeeController {
    
    private static final Logger LOG = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(method=RequestMethod.GET) 
    public List<Employee> findAll() {
        LOG.debug("findAll() request received");
        return employeeService.findAll();
    }

    @RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<Employee> save(@RequestBody Employee employee) {
        LOG.debug("save employee request received");
        return new ResponseEntity<Employee>(employeeService.save(employee), HttpStatus.CREATED);
    }

    @RequestMapping(path="/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Employee> update(@PathVariable String id, @RequestBody Employee employee) {
        LOG.debug("update request received");
        
        return new ResponseEntity<Employee>(employeeService.update(id, employee), HttpStatus.OK);
    }


    @RequestMapping(path="/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable String id) {
        LOG.debug("delete request received");
        employeeService.delete(id);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}
