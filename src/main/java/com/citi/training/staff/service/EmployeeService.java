package com.citi.training.staff.service;

import java.util.List;

import com.citi.training.staff.dao.EmployeeMongoRepo;
import com.citi.training.staff.model.Employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This is the main business logic class for Employees
 * 
 * @author Platypu
 * @see Employee
 */
@Component
public class EmployeeService {
    
    @Autowired
    private EmployeeMongoRepo mongoRepo;
    
    public List<Employee> findAll() {
        return mongoRepo.findAll();
    }
    
    public Employee save(Employee employee) {
        return mongoRepo.save(employee);
    }

    public Employee update(String id, Employee employee) {
        employee.setId(id);
        return mongoRepo.save(employee);
    }

    public void delete(String id) {
        mongoRepo.deleteById(id);
    }

    
}
